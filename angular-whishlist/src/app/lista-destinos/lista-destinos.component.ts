import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';

@Component({
selector: 'app-lista-destinos',
templateUrl: './lista-destinos.component.html',
styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

destinos: DestinoViaje[];
elementos: string[];

constructor() {
this.destinos = [];
this.elementos = ['Colombia','Brazil','Peru','Argentina']; 
}

ngOnInit(): void {
}

guardar(nombre:string, url:string):boolean {
this.destinos.push(new DestinoViaje(nombre, url));
console.log(new DestinoViaje(nombre, url))
return false;
}

elegido(d: DestinoViaje) {
    this.destinos.forEach(function(x) {x.setSelected(false)});
    d.setSelected(true);
}

}
